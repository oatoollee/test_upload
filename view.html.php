<?php

defined('_JEXEC') or die('Restricted access');

require_once JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_apiportal' . DS . 'helpers' . DS . 'apiconfiguration.php';
require_once JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_apiportal' . DS . 'models' . DS . 'apiportal.php';
require_once JPATH_SITE . DS . 'components' . DS . 'com_apiportal' . DS . 'models' . DS . 'apicatalog.php';
jimport('joomla.application.component.view');

class APIPortalViewApiTester extends JViewLegacy
{
    protected $apis;
    protected $access_token_data;
    protected $client_id;
    protected $client_secret;
    protected $redirect_uri;
    protected $apiManagerId;
    protected $currentApiId;
    protected $tab;
    protected $itemId;
    protected $usage;
    protected $apiRenderingTool;
    protected $tags;
    protected $menuId;

    public function display($tpl = null)
    {
        $jInput = JFactory::getApplication()->input;
        $this->type = $jInput->get('type', null, 'WORD');
        $this->tags = $jInput->get('tags', null, 'BASE64');
        $this->menuId = $jInput->get('menuId', null, 'INT');
        $document = JFactory::getDocument();

        // The request could come from unknown url, then use the this strange and bad workaround
        if (empty($this->menuId)) {
            $this->menuId = ApiportalHelper::getAllMenusParamValue($this->tags);
        }

        $this->apiRenderingTool = (int)ApiPortalHelper::renderingTool($this->menuId);
        if (empty($this->apiRenderingTool)) {
            $this->apiRenderingTool = $jInput->get('renderTool', null, 'INT');
        }

        //Optimized CSS/JS jquery, removing: components/com_apiportal/assets/js/jquery.min.js
        foreach ($document->_scripts as $key => $value) {
            if (strpos($key, 'jquery-noconflict') !== false) {
                //On the test page only the second element should be removed: ['/Joomla3f/media/jui/js/jquery-noconflict.js']
                unset($document->_scripts[$key]);
            }
        }

        $document->addScript('components/com_apiportal/assets/js/moment.min.js');
        $document->addScript('components/com_apiportal/assets/js/marked/lib/marked.js');
        $document->addScript('components/com_apiportal/assets/js/swagger/ui/handlebars-1.0.0-patched.js');

        // pso select application key text field
        $document->addScript('media/jui/js/chosen.jquery.min.js');
        $document->addStyleSheet('media/jui/css/chosen.css');
        $document->addStyleSheet('components/com_apiportal/assets/css/chosen-overrides.css');

        if ($this->apiRenderingTool === ApiPortalHelper::AMPLIFY_SWAGGER_UI_RENDERING_TOOL && $this->type == 'rest') {
            // ReactUI
            $document->addScript('components/com_apiportal/assets/react/dist/vendor.js');
            $document->addScript('components/com_apiportal/assets/react/dist/bundle.js');
            $document->addStyleSheet('components/com_apiportal/assets/react/dist/app.bundle.css');
        } else {
            $document->addScript('components/com_apiportal/assets/js/swagger/ui/jquery.slideto.min.js');
            $document->addScript('components/com_apiportal/assets/js/swagger/ui/jquery.wiggle.min.js');
            $document->addScript('components/com_apiportal/assets/js/swagger/ui/jquery.ba-bbq.min.js');
            $document->addScript('components/com_apiportal/assets/js/swagger/ui/underscore-min.js');
            $document->addScript('components/com_apiportal/assets/js/swagger/ui/backbone-min.js');

            $document->addScript('components/com_apiportal/assets/js/swagger/ui/btoa.js');
            $document->addScript('components/com_apiportal/assets/js/swagger/ui/highlight.7.3.pack.js');

            $document->addScript('components/com_apiportal/assets/js/swagger/shred.bundle.js');
            $document->addScript('components/com_apiportal/assets/js/swagger/swagger-oauth.js');
            $document->addScript('components/com_apiportal/assets/js/swagger/authorizations.js');
            $document->addScript('components/com_apiportal/assets/js/swagger/swagger.js');
            $document->addScript('components/com_apiportal/assets/js/swagger/swagger-ext.js');
            $document->addScript('components/com_apiportal/assets/js/swagger/ui/swagger-ui.js');
            $document->addScript('components/com_apiportal/assets/js/swagger/oauth-implicit.js');
            $document->addScript('components/com_apiportal/assets/js/swagger/oauth-clientcredentials.js');
            $document->addStyleSheet('components/com_apiportal/assets/css/swagger-screens.css');
            $document->addScript('components/com_apiportal/assets/js/swagger/swagger-load.js');

            //pso select application key text field
            $document->addScript('media/jui/js/chosen.jquery.min.js');
            $document->addStyleSheet('media/jui/css/chosen.css');
            $document->addStyleSheet('components/com_apiportal/assets/css/chosen-overrides.css');
            $this->apiRenderingTool = ApiPortalHelper::SWAGGER_UI_RENDERING_TOOL;
            $this->setLayout('swagger');
        }

        //Check for download error
        $downloadError = $jInput->get('fail', false, 'BOOLEAN');
        if ($downloadError) {
            JError::raiseWarning(null, JText::_('COM_APIPORTAL_APITEST_DOWNLOAD_ERROR'));
        }

        // check for errors
        if (count($errors = $this->get("Errors"))) {
            Jerror::raiseError(500, implode('<br />', $errors));

            return false;
        }

        $modelApiportal = new ApiportalModelapiportal();
        $this->proxyTimeout = $modelApiportal->getProxyTimeout();

        $session = JFactory::getSession();

        // OAuth access token
        $this->access_token_data = $session->get(ApiPortalSessionVariables::ACCESS_TOKEN_DATA);
        $this->client_id = $session->get(ApiPortalSessionVariables::CLIENT_ID);
        $this->client_secret = $session->get(ApiPortalSessionVariables::CLIENT_SECRET);
        $this->redirect_uri = $session->get(ApiPortalSessionVariables::REDIRECT_URI);
        $this->path = $session->get(ApiPortalSessionVariables::CURRENT_METHOD_PATH);
        $this->error = $session->get(ApiPortalSessionVariables::ERROR);
        $session->clear(ApiPortalSessionVariables::ACCESS_TOKEN_DATA);
        $session->clear(ApiPortalSessionVariables::CLIENT_ID);
        $session->clear(ApiPortalSessionVariables::CLIENT_SECRET);
        $session->clear(ApiPortalSessionVariables::REDIRECT_URI);
        $session->clear(ApiPortalSessionVariables::CURRENT_URL);
        $session->clear(ApiPortalSessionVariables::ERROR);

        // cross check for public mode user
        $currentSessionId = $session->get('user')->get('id');
        $appUserId = $session->get('appUserId');

        if ($appUserId != $currentSessionId && $session->get('PublicAPIMode', 0) == 1) {
            $app = JFactory::getApplication();
            $url = JRoute::_('index.php?option=com_users&view=login');
            $app->redirect($url, JText::_('JGLOBAL_SIGN_IN_REQUIRED'), 'error');
        }

        $catalog = new APIPortalModelApiCatalog();
        $this->apis = $catalog->getItems();
        $this->apiManagerId = $jInput->get('managerId', null, 'INT');

        //get api manager connection
        if (!is_null($this->apiManagerId)) {
            $mngrs = $session->get('activeManagers');
            $header = $mngrs[$this->apiManagerId]['headers'];

            //get connection
            $conn = ApiPortalHelper::_getConnection($mngrs[$this->apiManagerId]['host'],
                $mngrs[$this->apiManagerId]['port'], $mngrs[$this->apiManagerId]['verifySSL'],
                $mngrs[$this->apiManagerId]['verifyHost'], $mngrs[$this->apiManagerId]['isMaster'],
                $mngrs[$this->apiManagerId]['manager_id']);
            $this->connHeaders = $header;
            $this->conn = $conn;
        }

        $this->currentApiId = $jInput->get('apiId', null, 'STRING');
        $this->currentApiId = ApiPortalHelper::cleanHtml($this->currentApiId, false, true);
        $this->tab = $jInput->get('tab', null, 'STRING');
        $this->tab = ApiPortalHelper::cleanHtml($this->tab, false, true);
        $this->itemId = $jInput->get('Itemid', '', 'INT');
        $this->itemId = ApiPortalHelper::cleanHtml($this->itemId, false, true);
        $this->usage = $jInput->get('usage', 'api', 'ALNUM');
        $this->usage = ApiPortalHelper::cleanHtml($this->usage, false, true);

        // $isApplicationMenuItemPublished and $applications are used
        // to determine whether to show 'Create your first application' link
        if($this->tab == 'messages'){
            $this->isApplicationMenuItemPublished = ApiPortalHelper::isMenuItemPublished("index.php?option=com_apiportal&view=applications");
            $appicationsModel = JModelList::getInstance('applications', 'ApIPortalModel');
            $this->applications = $appicationsModel->getItems();
        }

        parent::display($tpl);
    }

    /**
     * Generate base64 encoded uri string for current page
     * It's used for SDK and swagger download as return uri on error
     * Also checks for fail param and removes it (this param is added from Sdk, swagger
     * download on error)
     * @return string
     */
    public function generateReturnUri()
    {
        //Get query from the uri
        $urlQuery = parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY);
        //Get path from the uri
        $urlPath = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        //Parse the uri query string
        parse_str($urlQuery, $urlQueryArray);
        //Remove if fail param exist
        if (array_key_exists('fail', $urlQueryArray)) {
            unset($urlQueryArray['fail']);
        }
        //Add the rest of the params to the path
        $urlPath = $urlPath . (!empty($urlQueryArray) ? '?' . http_build_query($urlQueryArray) : null);

        //return path as base64 string
        return base64_encode($urlPath);
    }
}
