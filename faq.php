<?php 
//require header files
require_once JPATH_SITE.'/components/com_apiportal/helpers/apiportal.php';

// Alias to redirect to respective pages
// API
$apiLink = ApiPortalHelper::getAliasToLinkMenus("index.php?option=com_apiportal&view=apicatalog");
$api = isset($apiLink[0]['alias']) ? $apiLink[0]['alias'] : '#';


// Pricing
$pricingLink = ApiPortalHelper::getAliasToLinkMenus("index.php?option=com_apiportal&view=pricing");
$pricing = isset($pricingLink[0]['alias']) ? $pricingLink[0]['alias'] : '#';

?>
<div class="head">
  <h1 class="auto">Frequently Asked Questions</h1>
<!--  
  <p class="auto"><em>Please email <a href='email'>faq@apiteam.thanachartbank.co.th</a> to have questions added here</em></p>
-->
</div>

<div class="body auto">

<h2><b>BANKS</b></h2>
<ul>
	<li><a href="index.php?option=com_content&view=article&id=5&catid=2">How do you ensure security?</a></li>
<!--
	<li><a href="index.php?option=com_content&view=article&id=5&catid=2">Is there a video I can watch that will help me?</a></li>
	<li><a href="index.php?option=com_content&view=article&id=5&catid=2">Where can I get help if I'm stuck?</a></li>
-->
</ul>
<p> </p>

<h2><b>DEVELOPER</b></h2>
<ul>
	<li><a href="index.php?option=com_content&view=article&id=6&catid=2">How do I start using the API?</a></li>
	<li><a href="index.php?option=com_content&view=article&id=8&catid=2">What can we do with the API?</a></li>
	<li><a href="index.php?option=com_content&view=article&id=9&catid=2">Which version of the API should I use?</a></li>
<!--	
<li><a href="index.php?option=com_content&view=article&id=5&catid=2">What is an S-API?</a></li>
	<li><a href="index.php?option=com_content&view=article&id=5&catid=2">How do I use the 'Tags' button to get APIs for a specific product?</a></li>
-->
</ul>
<p> </p>

<h2><b>FEATURES</b></h2>
<ul>
<li><a href="index.php?option=com_content&view=article&id=7&catid=2">What resources / features does the THANACHART API currently support?</a></li>
<!--	   	
<li><a href="index.php?option=com_content&view=article&id=5&catid=2">What is an API Key?</a></li>
	<li><a href="index.php?option=com_content&view=article&id=5&catid=2">Where do I get an API Key?</a></li>
	<li><a href="index.php?option=com_content&view=article&id=5&catid=2">What is OAuth?</a></li>
	<li><a href="index.php?option=com_content&view=article&id=5&catid=2">I'm getting a 401 HTTP Error when trying to access an API</a></li>
	<li><a href="index.php?option=com_content&view=article&id=5&catid=2">What is the default expiration time for my JWT token?</a></li>
	<li><a href="index.php?option=com_content&view=article&id=5&catid=2">How do I keep authentication tokens safe?</a></li>  
</ul>
<p> </p>

<h2><b>API Sandbox & Testing</b></h2>
<ul>
	<li><a href="index.php?option=com_content&view=article&id=5&catid=2">How do I use the API sandbox?</a></li>
	<li><a href="index.php?option=com_content&view=article&id=5&catid=2">How do I generate SDKs for my choice of programming language?</a></li>
	<li><a href="index.php?option=com_content&view=article&id=5&catid=2">What other ways of testing an API do I have?</a></li>
</ul>
<p> </p>

<h2><b>Developing APIs</b></h2>
<ul>
	<li><a href="index.php?option=com_content&view=article&id=5&catid=2">I built an API! How do I publish it in API Portal?</a></li>
</ul>
<p> </p>  
-->
</div>
