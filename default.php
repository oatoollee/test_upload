<?php
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.helper');
require_once JPATH_COMPONENT . '/views/apicatalog/view.html.php';

// Make sure the session is valid before displaying view
ApiPortalHelper::checkSession();

// Manage hidden tab for public API user
$publicApiAction = ApiPortalHelper::hasHiddenTabforPublicUser();

if (!defined('DS')) {
    define('DS', DIRECTORY_SEPARATOR);
}

require_once JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_apiportal' . DS . 'helpers' . DS . 'apiconfiguration.php';
require_once JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_apiportal' . DS . 'models' . DS . 'apiportal.php';

$tryItProxy = JURI::base(false) . 'index.php?option=com_apiportal&task=proxy.tryIt&ajax=1&' . JSession::getFormToken() . '=1';
$session = JFactory::getSession();
$basePath = ApiPortalHelper::getVersionedBaseFolder();
$swaggerVersion = ApiPortalHelper::getSwaggerVersion();

if (empty($this->menuId)) {
    $this->menuId = ApiportalHelper::getAllMenusParamValue($this->tags);
}

$showClientSdkButton = ApiPortalHelper::getClientSdkValue($this->menuId);
$showSwaggerButton = ApiPortalHelper::getSwaggerValue($this->menuId);
$enableInlineTryIt = ApiPortalHelper::getEnableInlineTryIt($this->menuId);
$swaggerColorPalette = ApiPortalHelper::getSwaggerColorPalette($this->menuId);

$methodColors = [];
if ($swaggerColorPalette == ApiportalHelper::SWAGGER_COLORFUL_PALETTE) {
    $methodColors = ApiPortalHelper::getMethodColors($this->menuId);
}

$hideTag = ApiportalHelper::getHideTags($this->menuId);
$hideTag = strtolower($hideTag);

if ($this->tab == null) {
    $this->tab = 'tests';
}

$mngrs = $session->get('activeManagers');
$headers = $mngrs[$this->apiManagerId]['headers'];
$managerTags = json_encode($mngrs[$this->apiManagerId]['tag']);
$managerTagsArr = $mngrs[$this->apiManagerId]['tag'];
$ajaxEndpointCall = JURI::base(false) . 'index.php?option=com_apiportal&task=oauth.requestToken&managerId=' . $this->apiManagerId;
APIPortalViewApiCatalog::sortApiList($this->apis, SORT_ASC);

// OAuth redirect url
$url = parse_url(JUri::base());
$scheme = isset($url['scheme']) ? $url['scheme'] : 'https';
$host = isset($url['host']) ? $url['host'] : null;
$port = isset($url['port']) ? $url['port'] : null;
$portalRedirectUrl = $scheme . '://' . $host . ($port ? ':' . $port : '') . '/cb';

$this->access_token_data = json_decode($this->access_token_data);
$appLink = ApiPortalHelper::getAliasToLinkMenus("index.php?option=com_apiportal&view=applications");
?>

<!-- Set some data needed for Swagger -->
<script type="text/javascript">
    //    Translations
    window.NOT_SUPPORTED_AUTH = "<?= JText::_('COM_APIPORTAL_APITEST_API_NOT_SUPPORT_AUTH') ?>";
    window.OVERRIDDEN_AUTH = "<?= JText::_('COM_APIPORTAL_APITEST_API_OVERRIDDEN_AUTH') ?>";
    window.OVERRIDDEN_AUTH_PASS_TROUGH = "<?= JText::_('COM_APIPORTAL_APITEST_API_OVERRIDDEN_AUTH_PASS_TROUGH') ?>";
    window.SPECIFY_CREDENTIALS = "<?= JText::_('COM_APIPORTAL_APITEST_API_SPECIFY_CREDENTIALS') ?>";
    window.NETWORK_ERROR = "<?= JText::_('COM_APIPORTAL_APITEST_API_NETWORK_ERROR') ?>";
    window.NO_APPLICATIONS_WITH_API_KEYS = "<?= JText::sprintf('COM_APIPORTAL_APITEST_API_NO_APPLICATIONS_WITH_API_KEYS',
        (isset($appLink[0]['alias']) ? $appLink[0]['alias'] : '#')) ?>";
    window.NO_APPLICATIONS_WITH_OAUTH_CREDENTIALS = "<?= JText::_('COM_APIPORTAL_APITEST_API_NO_APPLICATIONS_WITH_OAUTH_CREDENTIALS') ?>";
    window.LOAD_SWAGGER_ERROR = "<?= JText::_('COM_APIPORTAL_APITEST_API_LOAD_SWAGGER_ERROR') ?>";
    window.SELECT_SETTINGS = "<?= JText::_('COM_APIPORTAL_APITEST_API_SELECT_SETTINGS') ?>";
    window.API_KEY_FIELD = "<?= JText::_('COM_APIPORTAL_APITEST_API_KEY_FIELD') ?>";
    window.OAUTH_FIELD = "<?= JText::_('COM_APIPORTAL_APITEST_OAUTH_FIELD') ?>";
    window.BASIC_FIELD = "<?= JText::_('COM_APIPORTAL_APITEST_BASIC') ?>";
    window.BASIC_USERNAME = "<?= JText::_('COM_APIPORTAL_APITEST_BASIC_USERNAME') ?>";
    window.BASIC_PASSWORD = "<?= JText::_('COM_APIPORTAL_APITEST_BASIC_PASSWORD') ?>";
    window.OAUTH_FLOW_FIELD = "<?= JText::_('COM_APIPORTAL_APITEST_SELECT_OAUTH_FLOW_FIELD') ?>";
    window.SELECT_API_KEY_DEFAULT = "<?= JText::_('COM_APIPORTAL_APITEST_API_SELECT_API_KEY_DEFAULT') ?>";
    window.SELECT_OAUTH_CREDENTIALS_DEFAULT = "<?= JText::_('COM_APIPORTAL_APITEST_API_SELECT_OAUTH_CREDENTIALS_DEFAULT') ?>";
    window.SELECT_OAUTH_FLOW_DEFAULT = "<?= JText::_('COM_APIPORTAL_APITEST_API_SELECT_OAUTH_FLOW_DEFAULT') ?>";
    window.OAUTH_FLOW_CLIENT_CREDENTIALS = "<?= JText::_('COM_APIPORTAL_APITEST_API_OAUTH_FLOW_CLIENT_CREDENTIALS') ?>";
    window.OAUTH_FLOW_AUTHORIZATION_CODE = "<?= JText::_('COM_APIPORTAL_APITEST_API_OAUTH_FLOW_AUTHORIZATION_CODE') ?>";
    window.AUTHORIZATION_TOKEN_TEXT = "<?= JText::_('COM_APIPORTAL_APITEST_AUTHORIZATION_TOKEN') ?>";
    window.UNAUTHORIZED_TOKEN_TEXT = "<?= JText::_('COM_APIPORTAL_APITEST_UNAUTHORIZED_TOKEN') ?>";
    window.AUTHORIZE_DIALOG_REQUEST_TOKEN = "<?= JText::_('COM_APIPORTAL_APITEST_AUTHORIZE_DIALOG_REQUEST_TOKEN') ?>";
    window.AUTHORIZE_DIALOG_TITLE = "<?= JText::_('COM_APIPORTAL_APITEST_AUTHORIZE_DIALOG_TITLE') ?>";
    window.AUTHORIZE_DIALOG_MESSAGE = "<?= JText::_('COM_APIPORTAL_APITEST_AUTHORIZE_DIALOG_MESSAGE') ?>";
    window.AUTHORIZE_DIALOG_AUTH = "<?= JText::_('COM_APIPORTAL_APITEST_AUTHORIZE_DIALOG_AUTH') ?>";
    window.AUTHORIZE_DIALOG_CANCEL = "<?= JText::_('COM_APIPORTAL_APITEST_AUTHORIZE_DIALOG_CANCEL') ?>";
    window.AUTHORIZE_DIALOG_REQUEST_TOKEN_NO_CLIENT_ID = "<?= JText::_('COM_APIPORTAL_APITEST_AUTHORIZE_DIALOG_REQUEST_TOKEN_NO_CLIENT_ID') ?>";
    window.AUTHORIZE_DIALOG_REQUEST_TOKEN_NO_REDIRECT_URLS = "<?= JText::_('COM_APIPORTAL_APITEST_AUTHORIZE_DIALOG_REQUEST_TOKEN_NO_REDIRECT_URLS') ?>";
    window.AUTHORIZE_DIALOG_REQUEST_TOKEN_OK_BUTTON = "<?= JText::_('COM_APIPORTAL_APITEST_AUTHORIZE_DIALOG_REQUEST_TOKEN_OK_BUTTON') ?>";
    window.AUTHORIZE_DIALOG_DISMISS_TOKEN = "<?= JText::_('COM_APIPORTAL_APITEST_AUTHORIZE_DIALOG_DISMISS_TOKEN') ?>";
    window.API_REDIRECT_URL_UNSUPPORTED_MESSAGE = "<?= JText::_('COM_APIPORTAL_APITEST_API_REDIRECT_URL_UNSUPPORTED_MESSAGE') ?>";
    window.SELECT_AUTHENTICATION_TYPE_FIELD = "<?= JText::_('COM_APIPORTAL_APITEST_SELECT_AUTHENTICATION_TYPE_FIELD') ?>";
    window.SELECT_AUTHENTICATION_TYPE_DEFAULT = "<?= JText::_('COM_APIPORTAL_APITEST_SELECT_AUTHENTICATION_TYPE_DEFAULT') ?>";
    window.SELECT_AUTHENTICATION_TYPE_API_KEY = "<?= JText::_('COM_APIPORTAL_APITEST_SELECT_AUTHENTICATION_TYPE_API_KEY') ?>";
    window.SELECT_AUTHENTICATION_TYPE_OAUTH = "<?= JText::_('COM_APIPORTAL_APITEST_SELECT_AUTHENTICATION_TYPE_OAUTH') ?>";
    window.SELECT_AUTHENTICATION_TYPE_HTTP_BASIC = "<?= JText::_('COM_APIPORTAL_APITEST_SELECT_AUTHENTICATION_TYPE_HTTP_BASIC') ?>";
    //    Variables
    window.token = "<?= JSession::getFormToken() ?>";
    window.tags = "<?= implode(',', $managerTagsArr) ?>";
    window.hiddenTags = "<?= $hideTag ?>";
    window.portalRedirectUrl = "<?= $portalRedirectUrl ?>";
    window.accessToken = "<?= isset($this->access_token_data->access_token) ? $this->access_token_data->access_token : '' ?>";
    window.accessTokenType = "<?= isset($this->access_token_data->token_type) ? $this->access_token_data->token_type : '' ?>";
    window.clientId = "<?= $this->client_id ?>";
    window.clientSecret = "<?= $this->client_secret ?>";
    window.redirectUri = "<?= $this->redirect_uri ?>";
    window.path = "<?= $this->path ?>";
    window.enableInlineTryIt = "<?= $enableInlineTryIt ?>";
    window.swaggerColorPalette = "<?= $swaggerColorPalette ?>";
    window.methodColors = '<?= json_encode($methodColors); ?>';
    window.err = <?= json_encode(empty($this->error) ? [] : $this->error); ?>;
    window.proxyTimeout = <?= $this->proxyTimeout; ?>;
    window.menuId = <?= $this->menuId ?>;
    window.apiId = '<?= $this->currentApiId ?>';
</script>

<div class="head">
    <h1 class="auto">
        <span id="api-head-name"></span>
        <span id="api-head-version"></span>
    </h1>
    <p class="auto"><em><!-- placeholer --></em></p>
</div>

<div class="btn-toolbar" role="toolbar">
    <div class="auto">
        <div class="action-group object-chooser">
            <div class="dropdown sort-dropdown">
                <button id="api-nav" type="button" class="btn btn-default dropdown-toggle icon chevron-down"
                        data-toggle="dropdown">
                    <span id="api-nav-name"></span>
                    <span id="api-nav-version"></span>
                </button>
                <ul class="dropdown-menu" role="menu">
                    <?php foreach ($this->apis as $apiItemSort) : ?>
                        <li>
                            <a href="<?= JUri::base() ?>index.php?option=com_apiportal&view=apitester&usage=<?= $this->usage; ?>&apiName=<?= rawurlencode($apiItemSort['name']); ?>&sn=<?= rawurlencode($apiItemSort['name']); ?>&Itemid=<?= $this->itemId; ?>&tab=<?= $this->tab; ?>&apiId=<?= $apiItemSort['id'] ?>&menuId=<?php echo $this->menuId; ?><?php if (!empty($apiItemSort['apiVersion'])) {
                                echo "&apiVersion=" . rawurlencode($apiItemSort['apiVersion']);
                            } ?>&managerId=<?= $apiItemSort['managerId'] ?>&renderTool=<?= $this->apiRenderingTool ?>&type=<?= $apiItemSort['type'] ?>"
                               class="btn btn-primary"><?php echo $this->escape($apiItemSort['name']); ?><?php if (!empty($apiItemSort['apiVersion'])) {
                                    echo "- " . $this->escape($apiItemSort['apiVersion']);
                                } ?></a>
                        </li>
                    <?php endforeach ?>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="body auto">
    <div id="custom-message-container" class="hidden">
        <div id="system-message">
            <div class="alert alert-error">
                <a class="close" data-dismiss="alert">×</a>

                <h4 class="alert-heading"><?= JText::_('COM_APIPORTAL_APITEST_ERROR') ?></h4>
                <div>
                    <p id="system-message-text"></p>
                </div>
            </div>
        </div>
    </div>
    <!-- Main container - do not display until the ajax call for swagger definition is finished -->
    <div id="main-container" style="position: relative;">
        
        <div class="col-sm-12 row entry-details">
            <div class="col-sm-2">
                <div class="logo" id="api-image"
                     style="background-image: url('<?= JURI::root() ?>components/com_apiportal/assets/img/no_image_loading.png');"></div>
            </div>
            <!-- .col-sm-2 -->

        <div class="col-sm-10 content">
            <div class="row control-group">
                <label class="col-sm-3 control-label" for="version"><?= JText::_('COM_APIPORTAL_APITEST_VERSION') ?></label>
                <!-- Version and deprecated label - if exist -->
                <div class="col-sm-8" id="version"></div>
            </div>
            <div class="row control-group">
                <label class="col-sm-3 control-label" for="host"><?= JText::_('COM_APIPORTAL_APITEST_HOST') ?></label>
                <div class="col-sm-8" id="host"></div>
            </div>
            <div class="row control-group">
                <label class="col-sm-3 control-label" for="basepath"><?= JText::_('COM_APIPORTAL_APITEST_BASE_PATH') ?></label>
                <div class="col-sm-8" id="basepath"></div>
            </div>
            <div class="row control-group">
                <label class="col-sm-3 control-label" for="cors"><?= JText::_('COM_APIPORTAL_APITEST_CORS') ?></label>
                <div class="col-sm-8" id="cors"></div>
            </div>
            <div class="row control-group" id = "tagDiv">
                <label class="col-sm-3 control-label" for="tags"><?= JText::_('COM_APIPORTAL_APITEST_TAGS') ?></label>
                <div class="col-sm-8" id="tags"></div>
            </div>
            <div class="row control-group">
                <label class="col-sm-3 control-label" for="api-type"><?= JText::_('COM_APIPORTAL_APITEST_TYPE') ?></label>
                <div class="col-sm-8" id="api-type"></div>
            </div>
            <?php if ($showSwaggerButton == true) { ?>
                <div class="row control-group">
                    <label class="col-sm-3 control-label"
                           for="api-definition"><?= JText::_('COM_APIPORTAL_APITEST_DOWNLOAD') ?></label>
                    <div class="col-sm-8" id="api-definition"></div>
                </div>
            <?php } ?>
            <?php if ($showClientSdkButton == true) { ?>
                <div class="row control-group" id="sdk-group">
                    <label class="col-sm-3 control-label" for="api-sdks"><?= JText::_('COM_APIPORTAL_APITEST_SDK') ?></label>
                    <div class="col-sm-8" id="api-sdks"></div>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="overlay" style="display: none;"></div>
    <div class="clearfix"></div>
	<div class="description markdown-reset" id="api-description"></div>
<!-- This is closing from another page -->
</div>
<div class="clearfix"></div>
<div class="tabs" id="tabs">
    <ul class="nav nav-tabs">
        <li <?php echo($this->tab === 'tests' ? 'class="active"' : ''); ?>><a
                href="<?= JUri::base() ?>index.php?option=com_apiportal&view=apitester&tab=tests&apiId=<?= $this->currentApiId ?>&menuId=<?=$this->menuId?>&managerId=<?= $this->apiManagerId ?>&renderTool=<?= $this->apiRenderingTool ?>&type=<?= !is_null($this->type) ? $this->type : '' ?>"
                data-toggle="TODO"><?= JText::_('COM_APIPORTAL_APITEST_TAB_TEST') ?></a></li>

        <li <?php echo($this->tab !== null && $this->tab !== '' && $this->tab !== 'tests' ? 'class="active"' : ''); echo $publicApiAction;   ?>><a
                href="<?= JUri::base() ?>index.php?option=com_apiportal&view=apitester&tab=messages&apiId=<?= $this->currentApiId ?>&menuId=<?php echo $this->menuId; ?>&managerId=<?= $this->apiManagerId ?>&renderTool=<?= $this->apiRenderingTool ?>&type=<?= !is_null($this->type) ? $this->type : '' ?>"
                data-toggle="TODO"><?= JText::_('COM_APIPORTAL_APITEST_TAB_USAGE'); ?></a></li>
    </ul>
    <div class="tab-content" id="tabs-content">
        <!-- API Tester Tab -->

            <div class="tab-pane fade <?php echo($this->tab === null || $this->tab === '' || $this->tab === 'tests' ? 'in active' : ''); ?>"
                 id="tests">
                <div id="message-bar" class="swagger-ui-wrap alert alert-danger alert-dismissable hidden"></div>
                <div id="swagger-ui-container" class="swagger-ui-wrap"></div>
            </div>
            <!-- Metrics Tab -->
            <div class="tab-pane fade <?php echo($this->tab !== null && $this->tab !== '' && $this->tab !== 'tests' ? 'in active' : ''); ?>"
                 id="metrics">
                <?php if ($this->tab !== null && $this->tab !== '' && $this->tab !== 'tests') { ?>
                    <?php include JPATH_COMPONENT . '/views/monitoring/tmpl/default.php'; ?>
                <?php } ?>
            </div>
            <!-- .tab-pane -->
        </div>
        <!-- .tab-content -->
    </div>
</div>
