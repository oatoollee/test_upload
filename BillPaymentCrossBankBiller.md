	This API uses for Payment Status Checking as below steps:

	1.) Request Access Token with the following path "/api/oauth/token". It will return with "access_token" in json body response format.

	2.) Request BillPayment Status by using "access_token" in step 1 with the following path "/payment/paymentexecution/biller/billpayments/status".

